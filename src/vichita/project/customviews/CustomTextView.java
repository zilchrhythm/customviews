package vichita.project.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class CustomTextView extends TextView {
	
	public CustomTextView(Context context) {
		this(context,null);
	}
	
	public CustomTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		if (isInEditMode()) {
			return ;
		}
		
		TypedArray styledAttrs = context.obtainStyledAttributes(attrs, R.styleable.CustomTextView);
		String fontName = styledAttrs.getString(R.styleable.CustomTextView_typeface);
		styledAttrs.recycle();
		
		if (fontName!=null) {
			Typeface typeface = Typeface.createFromAsset(context.getAssets(), fontName);
			setTypeface(typeface);
		}
	}
	
	public CustomTextView(Context context, AttributeSet attrs, int defStyle) {
		this(context, attrs);
	}
	
	public void setTypeFace(String path){
		String fontName = path;
		if (fontName!=null) {
			Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), fontName);
			setTypeface(typeface);
		}
	}
}
