package vichita.project.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class CustomSwitch extends LinearLayout {

	private RelativeLayout track;
	private ImageButton thumb;
	private Animation slideLeftAni,slideRightAni;
	private boolean isChecked;
	
	public CustomSwitch(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomSwitch, 0, 0);
		int thumbId = a.getResourceId(R.styleable.CustomSwitch_thumb,R.drawable.switch_thumb);
		int trackId = a.getResourceId(R.styleable.CustomSwitch_track, 0);
		isChecked = a.getBoolean(R.styleable.CustomSwitch_enable, false);
		
		a.recycle();
		
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.switch_view, this, true);
		
		track = (RelativeLayout) getChildAt(0);
		
		thumb = (ImageButton) track.getChildAt(0);
		thumb.setImageResource(thumbId);
		
		slideLeftAni = AnimationUtils.loadAnimation(context, R.animator.slide_left);
		slideLeftAni.setAnimationListener(new CustomAnimationListener(R.drawable.switch_track_gray));

		slideRightAni = AnimationUtils.loadAnimation(context, R.animator.slide_right);
		slideRightAni.setAnimationListener(new CustomAnimationListener(trackId));

		setOnTouchListener(new CustomTouchListener());
		thumb.setOnTouchListener(new CustomTouchListener());
		
		checkEnable();
	}
	
	public CustomSwitch(Context context) {
		super(context, null);
	}
	
	public void setThumb(int resId){
		thumb.setImageResource(resId);
	}
	
	public void setTrack(int resId){
		track.setBackgroundResource(resId);
	}
	
	public void setChecked(boolean enable){
		isChecked = enable;
		checkEnable();
	}
	
	public boolean isChecked() {
		return isChecked;
	}
	
	private void checkEnable(){
		RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) thumb.getLayoutParams();
		if (isChecked) {
			params.addRule(RelativeLayout.ALIGN_PARENT_LEFT,0);
			params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT,RelativeLayout.TRUE);
			thumb.startAnimation(slideRightAni);
		}else{					
			params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT,0);
			params.addRule(RelativeLayout.ALIGN_PARENT_LEFT,RelativeLayout.TRUE);
			thumb.startAnimation(slideLeftAni);
		}
		
		thumb.setLayoutParams(params);
	}
	
	private class CustomAnimationListener implements AnimationListener{

		int resId;
		
		public CustomAnimationListener(int resId) {
			this.resId = resId;
		}
		
		@Override
		public void onAnimationEnd(Animation animation) {
			track.setBackgroundResource(resId);
		}

		@Override
		public void onAnimationRepeat(Animation animation) {
		}

		@Override
		public void onAnimationStart(Animation animation) {
		}
		
	}
	
	private class CustomTouchListener implements OnTouchListener{

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				setChecked(!isChecked);
				break;
			default:
				break;
			}
			return true;
		}
		
	}

}
