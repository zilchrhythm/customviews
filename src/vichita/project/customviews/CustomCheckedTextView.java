package vichita.project.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.CheckedTextView;

public class CustomCheckedTextView extends CheckedTextView {

	public CustomCheckedTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		if (isInEditMode()) {
			return ;
		}
		
		TypedArray styledAttrs = context.obtainStyledAttributes(attrs, R.styleable.CustomCheckedTextView);
		String fontName = styledAttrs.getString(R.styleable.CustomCheckedTextView_checkedtextview_typeface);
		styledAttrs.recycle();
		
		if (fontName!=null) {
			Typeface typeface = Typeface.createFromAsset(context.getAssets(), fontName);
			setTypeface(typeface);
		}
	}

}
