package vichita.project.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.EditText;

public class CustomEditText extends EditText {

	private int errorbackgroundRedId;
	private Drawable defaultBackgroud;

	public CustomEditText(Context context, AttributeSet attrs) {
		super(context, attrs);

		if (isInEditMode()) {
			return;
		}

		TypedArray styledAttrs = context.obtainStyledAttributes(attrs,
				R.styleable.CustomEditText);
		String fontName = styledAttrs.getString(R.styleable.CustomEditText_edittext_typeface);
		errorbackgroundRedId = styledAttrs.getResourceId(R.styleable.CustomEditText_errorBackground, 0);
		styledAttrs.recycle();

		if (fontName != null) {
			Typeface typeface = Typeface.createFromAsset(context.getAssets(),
					fontName);
			setTypeface(typeface);
		}
		
		defaultBackgroud = getBackground();
	}

	public void setError(boolean error) {
		if (!error) {
			setBackground(defaultBackgroud);
		}else{
			setBackgroundResource(errorbackgroundRedId);
		}
	}

}
