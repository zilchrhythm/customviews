package vichita.project.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

public class CustomButton extends Button {
	public CustomButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		if (isInEditMode()) {
			return ;
		}
		
		TypedArray styledAttrs = context.obtainStyledAttributes(attrs, R.styleable.CustomButton);
		String fontName = styledAttrs.getString(R.styleable.CustomButton_button_typeface);
		styledAttrs.recycle();
		
		if (fontName!=null) {
			Typeface typeface = Typeface.createFromAsset(context.getAssets(), fontName);
			setTypeface(typeface);
		}
	}
}
